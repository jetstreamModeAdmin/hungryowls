//
//  ViewController.swift
//  HungryOwls
//
//  Created by ranger on 3/26/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

import UIKit

class MotherViewController: UIViewController, OwlProtocol {
    
    
    let motherImage = UIImage(named: "mother")
    var _motherImage = UIImageView()
    
    
    var _momMessagePane = UILabel()
    
    var feederOne: UIButton!
    var feederTwo: UIButton!
    var feederThree: UIButton!
    
    let owl1 = OwlViewController(owlName: "kid1")
    let owl2 = OwlViewController(owlName: "kid2")
    let owl3 = OwlViewController(owlName: "kid3")
    
    let cageImage = UIImage(named: "cage")
    var _cageImage = UIImageView()
    
    let cageMouse = UIImage(named: "cageMouse")
    var _cageMouse = UIImageView()
    var _cageMouse1 = UIImageView()
    var _cageMouse2 = UIImageView()
    
    let spaceFood = UIImage(named: "food")
    var _spaceFood = UIImageView()
    
    var _pendulum: Int = 0
    
    var destPoint = CGPoint()
    
    func onFeedOneTap(sender: UIButton) {
        //println("feed 1 tapped")
        
        destPoint.x = owl1.view.frame.origin.x
        destPoint.y = owl1.view.frame.origin.y
        
        self.sendFoodToKid(destPoint, theOwl: owl1);
        
    }
    func onFeedTwoTap(sender: UIButton) {
        //println("feed 2 tapped")
        destPoint.x = owl2.view.frame.origin.x
        destPoint.y = owl2.view.frame.origin.y
        
        self.sendFoodToKid(destPoint, theOwl: owl2);
        
    }
    func onFeedThreeTap(sender: UIButton) {
        //println("feed 3 tapped")
        destPoint.x = owl3.view.frame.origin.x
        destPoint.y = owl3.view.frame.origin.y
        
        self.sendFoodToKid(destPoint, theOwl: owl3);
        
    }

    func sendMessageToMom(owl: OwlViewController, theMessage: String) {
        println("received owl message in mom = \(theMessage)")
        
        _momMessagePane.text = theMessage
    }

    
    func runInCage(cageMouse: UIImageView) {
        var randomX = CGFloat((arc4random()%44)+1)
        var xPos: CGFloat = 5 + randomX
        
        var r = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        var tm: NSTimeInterval = Double(round(10*r)/10)
        
        var dur: NSTimeInterval = Double(round(10*r)/10) + 0.3
        
        //println("rdelay =\(tm)")
        
        //println("dur =\(dur)")
        
        UIView.animateWithDuration(dur, delay: tm, options: nil, animations: {
            
            cageMouse.frame = CGRect(x: xPos, y: cageMouse.frame.origin.y, width: 41/2, height: 41/2)
            
            }, completion: {
                (value: Bool) in
                self.runInCage(cageMouse)
        })
        
        
        
    }
    
    func rotateImage(dangleMouse: UIImageView) {
        var randRotation = CGFloat(arc4random()%30)
        var degrees: CGFloat
        
        if (_pendulum == 0) {
            _pendulum = 1
            degrees = randRotation
        } else {
            _pendulum = 0
            degrees = -randRotation
        }
        
        var radians = Double(degrees) / 180.0 * M_PI
        
        UIView.animateWithDuration(1.0, delay: 0.0, options: nil, animations: {
            
            dangleMouse.transform = CGAffineTransformMakeRotation(CGFloat(radians))
            
            }, completion: {
                (value: Bool) in
                self.rotateImage(dangleMouse)
        })
        
    }
    func sendFoodToKid(dPoint: CGPoint, theOwl: OwlViewController) {
        _momMessagePane.text = "feeding"
        
        self.spawnFood()
        
        UIView.animateWithDuration(0.6, delay: 0.0, options: nil, animations: {
            
            self._spaceFood.frame = CGRect(x: dPoint.x+25, y: dPoint.y+40, width: 10, height: 10)
            
            }, completion: {
                (value: Bool) in
                
                self._spaceFood.removeFromSuperview()
                
                theOwl.communicationChannelDirect("eat up")
                
                self._momMessagePane.text = ".zz.zzz.z..."
                
                self.spawnFood()
                
                self.rotateImage(self._spaceFood)
                
                
        })
        
    }
    
    func spawnFood() {
        _spaceFood.removeFromSuperview()
        
        _spaceFood = UIImageView(image: spaceFood)
        _spaceFood.frame = CGRect(x: 148, y: 93, width: 68/2, height: 120/2)
        _spaceFood.layer.anchorPoint = CGPointMake(0.5, 0.0)
        self.view.addSubview(_spaceFood)
        
    }
    
    
    required init(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        _motherImage = UIImageView(image: motherImage)
        _motherImage.frame = CGRect(x: 238/2, y: 40/2, width: 463/2, height: 725/2)
        self.view.addSubview(_motherImage)
        
        _spaceFood = UIImageView(image: spaceFood)
        _spaceFood.frame = CGRect(x: 148, y: 93, width: 68/2, height: 120/2)
        _spaceFood.layer.anchorPoint = CGPointMake(0.5, 0.0)
        self.view.addSubview(_spaceFood)
        
        //[self rotateImage:_spaceFood duration:1.0 curve:UIViewAnimationCurveEaseOut degrees:45];
        
        self.rotateImage(_spaceFood)
        
        _cageImage = UIImageView(image: cageImage)
        _cageImage.frame = CGRect(x: 15, y: 25, width: 165/2, height: 140/2)
        self.view.addSubview(_cageImage)
        
        _cageMouse = UIImageView(image: cageMouse)
        _cageMouse.frame = CGRect(x: 50, y: 20, width: 41/2, height: 41/2)
        _cageImage.addSubview(_cageMouse)
        self.runInCage(_cageMouse)
        
        _cageMouse1 = UIImageView(image: cageMouse)
        _cageMouse1.frame = CGRect(x: 5, y: 30, width: 41/2, height: 41/2)
        _cageImage.addSubview(_cageMouse1)
        self.runInCage(_cageMouse1)
        
        _cageMouse2 = UIImageView(image: cageMouse)
        _cageMouse2.frame = CGRect(x: 25, y: 25, width: 41/2, height: 41/2)
        _cageImage.addSubview(_cageMouse2)
        self.runInCage(_cageMouse2)
        
        
        _momMessagePane = UILabel(frame: CGRect(x: 0, y: 75, width: 375, height: 30))
        _momMessagePane.text = "...sleeping..."
        //_momMessagePane.backgroundColor = UIColor.grayColor()
        _momMessagePane.textAlignment = .Center
        self.view.addSubview(_momMessagePane)
        
        let image = UIImage(named: "btnbg") as UIImage?
        
        feederOne = UIButton.buttonWithType(.System) as? UIButton
        feederOne.frame = CGRect(x: 180, y: 210, width: 80, height: 40)
        feederOne.setTitleColor(UIColor(red:0/255, green:0/255,blue:0/255,alpha:1.0), forState: .Normal)
        feederOne.setTitle("FEED 1", forState: .Normal)
        feederOne.setBackgroundImage(image, forState: .Normal)
        //feederOne.setTitle("yum 1", forState: .Highlighted)
        feederOne.addTarget(self, action: "onFeedOneTap:", forControlEvents: .TouchUpInside)
        self.view.addSubview(feederOne)
        
        feederTwo = UIButton.buttonWithType(.System) as? UIButton
        feederTwo.frame = CGRect(x: 180, y: 260, width: 80, height: 40)
        feederTwo.setTitleColor(UIColor(red:0/255, green:0/255,blue:0/255,alpha:1.0), forState: .Normal)
        feederTwo.setTitle("FEED 2", forState: .Normal)
        feederTwo.setBackgroundImage(image, forState: .Normal)
        //feederOne.setTitle("yum 2", forState: .Highlighted)
        feederTwo.addTarget(self, action: "onFeedTwoTap:", forControlEvents: .TouchUpInside)
        self.view.addSubview(feederTwo)
        
        feederThree = UIButton.buttonWithType(.System) as? UIButton
        feederThree.frame = CGRect(x: 180, y: 310, width: 80, height: 40)
        feederThree.setTitleColor(UIColor(red:0/255, green:0/255,blue:0/255,alpha:1.0), forState: .Normal)
        feederThree.setTitle("FEED 3", forState: .Normal)
        feederThree.setBackgroundImage(image, forState: .Normal)
        //feederOne.setTitle("yum 3", forState: .Highlighted)
        feederThree.addTarget(self, action: "onFeedThreeTap:", forControlEvents: .TouchUpInside)
        self.view.addSubview(feederThree)
        
        
        //owl1
        owl1.view.frame = CGRect(x: 15, y: 300, width: 100, height: 148)
        owl1.delegate = self;
        self.view.addSubview(owl1.view)
        
        //owl2
        owl2.view.frame = CGRect(x: 200, y: 390, width: 100, height: 148)
        owl2.delegate = self;
        self.view.addSubview(owl2.view)
        
        //owl3
        owl3.view.frame = CGRect(x: 50, y: 450, width: 100, height: 148)
        owl3.delegate = self;
        self.view.addSubview(owl3.view)
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

