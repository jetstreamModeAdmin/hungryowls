//
//  ChildView.swift
//  HungryOwls
//
//  Created by ranger on 3/9/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

import UIKit

protocol OwlProtocol {
    func sendMessageToMom(owl: OwlViewController, theMessage: String)
}

class OwlViewController: UIViewController, HungerMeterProtocol {
    
    var delegate:OwlProtocol?
    var label:String?
    
    //var childImage:UIImage
    //var _childImage = UIImageView()
    let owlImage0 = UIImage(named: "owl0")
    let owlImage1 = UIImage(named: "owl1")
    let owlImage2 = UIImage(named: "owl2")
    let _owlImage = UIImageView()
    
    let display = UILabel()
    
    //percentage hunger
    var hungerPercentage: CGFloat = 1
    
    let _myHungerMeter = HungerMeter()
    
    //timer
    var hungerTimer = NSTimer()
    
    init(owlName : String) {
        super.init(nibName: nil, bundle: nil)

        label = owlName
    }

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func communicationChannelDirect(message: String) {
        display.text = message
        
        _myHungerMeter.resetLife()
        
        self.fadeUpOwl()
        
        if (hungerTimer.valid) {
            
        } else {
            hungerPercentage = 1
            self.startUPTimer()
        }
        
        
        
    }
    func fadeUpOwl() {
        UIView.animateWithDuration(0.3, delay: 0.0, options: nil, animations: {
            
            self._owlImage.alpha = 1
            
            self._owlImage.frame = CGRect(x: 0, y: 0, width: 170/2, height: 190/2)
            
            }, completion: {
                (value: Bool) in
                
                self.scaleDownOwl()
        })
        
    }
    
    func scaleDownOwl() {
        UIView.animateWithDuration(0.6, delay: 0.0, options: nil, animations: {
            
            
            self._owlImage.frame = CGRect(x: 0, y: 0, width: 150/2, height: 166/2)
            
            }, completion: {
                (value: Bool) in
                
        })
    }
    

    func updateHungerToOwl(hungerAmount: CGFloat) {
        //println("amt in owl from hm = \(hungerAmount)")

        var hungerTotal = _myHungerMeter.frame.size.height

        hungerPercentage = (hungerAmount/hungerTotal)*100

        _owlImage.alpha = hungerAmount/hungerTotal

        if (_owlImage.alpha == 0) {
            _owlImage.alpha = 0.1
        }

        var hungerMessage: String
        
        if (hungerPercentage > 89 && hungerPercentage <= 100) {
            hungerMessage = "i'm full";
        } else if (hungerPercentage > 79 && hungerPercentage < 90) {
            hungerMessage = "just ate";
        } else if (hungerPercentage > 69 && hungerPercentage < 80) {
            hungerMessage = "i could eat";
        } else if (hungerPercentage > 59 && hungerPercentage < 70) {
            hungerMessage = "when is lunch?";
        } else if (hungerPercentage > 49 && hungerPercentage < 60) {
            hungerMessage = "french fries, yes.";
        } else if (hungerPercentage > 39 && hungerPercentage < 50) {
            hungerMessage = "low blood sugar :(";
        } else if (hungerPercentage > 29 && hungerPercentage < 40) {
            hungerMessage = "crap, so hungry";
        } else if (hungerPercentage > 19 && hungerPercentage < 30) {
            hungerMessage = "no love from mom";
        } else if (hungerPercentage > 9 && hungerPercentage < 20) {
            hungerMessage = "writing my will";
        } else if (hungerPercentage > 0 && hungerPercentage < 10) {
            hungerMessage = "pretty much dead";
        } else {
            hungerMessage = "R.I.P";

            _myHungerMeter.endLifeCheck()
            
            hungerTimer.invalidate()
        }
        
        display.text = hungerMessage

    }



    func startUPTimer() {
        //var randomInterval:NSTimeInterval = arc4random()%5
        let randomInterval = NSTimeInterval(CGFloat(Int(arc4random()%5)))
        
        hungerTimer = NSTimer.scheduledTimerWithTimeInterval(randomInterval, target: self, selector: Selector("timerFired"), userInfo: nil, repeats: true)
        
    }
    
    func timerFired() {
        //println("timer = \(label)")
        
        //println("received child message in mom = \(theMessage)")
        if (hungerPercentage > 0) {
            _myHungerMeter.setHungerAmount(5)
        } else {
            hungerTimer.invalidate()
        }
    }

/*
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
*/


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        if (label == "kid1") {
            _owlImage.image = owlImage0
        } else if (label == "kid2") {
            _owlImage.image = owlImage1
        } else {
            _owlImage.image = owlImage2
        }
                
        _owlImage.frame = CGRect(x: 0, y: 0, width: 150/2, height: 166/2)
        self.view.addSubview(_owlImage)


        
        display.frame = CGRect(x: 0, y: 90, width: 150, height: 30)
        display.text = "Missing Mom"
        //display.backgroundColor = UIColor.grayColor()
        display.textColor = UIColor(red:47/255, green:200/255,blue:168/255,alpha:1.0)
        display.textAlignment = .Left
        self.view.addSubview(display)
        
        _myHungerMeter.frame = CGRect(x: 0, y: 20, width: 6, height: 70)
        _myHungerMeter.label = label
        _myHungerMeter.delegate = self
        self.view.addSubview(_myHungerMeter)
        
        self.startUPTimer()
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        var theCry: String
        
        if (label == "kid1") {
            theCry = "owl 1 is starving"
        } else if (label == "kid2") {
            theCry = "owl 2 is hungry"
        } else if (label == "kid3") {
            theCry = "owl 3 must eat"
        } else {
            theCry = "random unknown cry"
        }
        
        delegate?.sendMessageToMom(self, theMessage: theCry)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
